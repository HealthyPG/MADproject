## **Steps to compile and run code**

### **Step 1: Install required librarys in Ubuntu**

> #### *Install Libjson and Libconfig librarys*
> #### *Run these commands one by one in terminal*

#### *To install Libjson (library to parse json file) library*
```
sudo apt-get update
sudo apt-get install libjson-c-dev
```
#### *To install Libconfig (library to parse configuration file) library*
```
sudo apt-get update
sudo apt-get install libconfig-dev
```

### **Step 2: Compile code**

> #### *Run these commands one by one in terminal*
```
gcc WIFI_config.c -ljson-c -lconfig -o config
./config
```