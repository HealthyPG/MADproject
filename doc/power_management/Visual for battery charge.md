## **Visual for Battery Charge**

## Using Battery datasheet specifications

In battery datasheet, discharge characteristics of that battery are present either in form of Voltage vs Duration graph or Voltage vs Battery percentage graph.

#### Steps to be followed:
  1. *Find the datsheet of the battery used in device*    
  Here we use an example of Li-ion battery which is widely used in Smart wearables and IoT devices

  2. *Import the Discharge characteristics of the battery*
     Here we select typical discharge characteristics of Li-ion batteries

  ![](https://siliconlightworks.com/image/data/Info_Pages/Li-ion%20Discharge%20Voltage%20Curve%20Typical.jpg)  
  
  3. *Create the graphical data in CSV format.*   
     For this we use an open web application [WEbPlotDigitizer](https://apps.automeris.io/wpd/) 
     Load our plot in application and follow the steps as shown in this tutorial 
     [Convert plot in CSV values](https://www.youtube.com/watch?v=LY3uNaij9Tg&t=282s)

     Load the plot and set X1,X2, Y1, Y2 values:

     ![](doc/power_management/Convert_Plot_into_CSV_step1.png)

     Extract points:

     ![](doc/power_management/Convert_plot_into_CSV_step2.png)

     Convert values in CSV format:

     ![](doc/power_management/Convert_plot_into_CSV_step4.png)

  4. *(Additional step) Convert data into suitable format*
     In case the datasheet provides voltage vs Battery( in mAh), convert battery value in percentage using the formula:   
     Battery % = (Current battery level / Maximum battery capacity) * 100%   
     In case the x-axis of the plot is duration, convert it into state of charge using formula:   
     SOC% = 100 - (Current duration/Maximum duration)*100%

  5. *C Program to take CSV file as input and voltage of device and output battery percentage*   
     For every time interval (say 120 sec), check the voltage value of the device and output battery percentage.
  6. *(Additional step) Use the battery percentage to convert into visual display.*    

#### Pros
 - Easy method to find battery vs voltage values without using external circuit.
 - C- code imlementation possible

#### Cons
 - Dependent on other web application
 - Although one time task, finding discharge characteristics of battery can be tedious.


## Using Grafana to display Battery Charge level

 This is one method of using the .csv file genrated to implement visuals of battery charge using Grafana and Influxdb.

 #### Steps to be followed:
 1. Install Grafana and Influxdb plugins in your system.
 2. Create bucket in Influxdb and add csv file in the ...
 3. Create a dashboard in Grafana and attach Influxdb file to it.
 4. Add a *Bar Gauge* and setup the basic features.
 5. For a demo purpose, I read the battery status of my laptop and display it on dashboard after every 30s of Interval.
 6. (Additional step) In the final device, read the voltage values incase battery value is not directly given and convert it SoC and display battery charge on laptop.

**References**- [Setup bar gauge in Grafana](https://www.youtube.com/watch?v=4qpI4T6_bUw&t=653s)

***Final result( Not exact, reference):***

![](https://grafana.com/api/dashboards/12542/images/8434/image)

## Display battery charge level using LEDs 
If hardaware implementation of the battery charge is required, this method uses LM3914 IC to connect the device to implement battery charge indicator

[Follow this tutorial](https://www.electronicshub.org/battery-level-indicator)

##### Note:
Grafana is best suited to display battery charge visual of the device. I have not implemented the LM3914 circuit mentioned in this summary although it can be quite useful in case of hardware implementation. 







