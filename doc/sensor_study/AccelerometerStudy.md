# Accelerometer

- An accelerometer is a transducer that is used to measure the physical or measurable acceleration that is made by an object. Co-ordinate acceleration cannot be measured using this device.
- It is an electro-mechanical device that is used to measure the specific force of an object, a force obtained due to the phenomenon of weight exerted by an object that is kept in the frame of reference of the accelerometer.
- In the case of static acceleration, the device is mainly used to find the degrees at which an object is tilted with respect to the ground. In dynamic acceleration, the movement of the object can be foreseen.


## Working

- **Principal Operation of Accelerometer**

![Operation](https://www.thepocketlab.com/sites/default/files/inline-images/accel%20model.png)

- The most commonly used device is the piezoelectric accelerometer. As the name suggests, it uses the principle of piezoelectric effect. The device consists of a piezoelectric quartz crystal on which an accelerative force, whose value is to be measured, is applied.
- Due to the special self-generating property, the crystal produces a voltage that is proportional to the accelerative force. The working and the basic arrangement is shown in the figure below.

## Piezoelectric Accelerometer
- As the device finds its application as a highly accurate vibration measuring device, it is also called a vibrating sensor. - Vibration sensors are used for the measurement of vibration in bearings of heavy equipment and pressure lines. 
- The piezoelectric accelerometer can be classified into two. 
  They are high impedance output accelerometer and low impedance accelerometer.

    1. In the case of high impedance device, the output voltage generated in proportion to the acceleration is given as the    input to other measuring instruments. The instrumentation process and signal conditioning of the output is considered high and thus a low impedance device cannot be of any use for this application. This device is used at temperatures greater than 120 degree Celsius.

    2. The low impedance accelerometer produces a current due to the output voltage generated and this charge is given to a FET transistor so as to convert the charge into a low impedance output. This type of accelerometers is most commonly used in industrial applications.

## Types

![Comparison](https://image.slidesharecdn.com/advantagecapacitivememsvsothertechnologiesfinal-150505041818-conversion-gate02/95/advantage-of-capacitive-mems-accelerometers-vs-other-technologies-9-638.jpg?cb=1462802650)


In industrial applications, the most commonly used components to convert the mechanical action into its corresponding electrical output signal are piezoelectric, piezoresistive and capacitive in nature. Piezoelectric devices are more preferred in cases where it is to be used in very high temperatures, easy mounting and also high frequency rang e up to 100 kilohertz.
- Piezoresistive devices are used in sudden and extreme vibrating applications. Capacitive accelerometers are preferred in applications such as a silicon-micro machined sensor material and can operate in frequencies up to 1 kilohertz. All these devices are known to have very high stability and linearity.
![PiezoA](https://www.instrumentationtoday.com/wp-content/uploads/2011/08/Piezoelectric-Accelerometer.jpg)

- Micro Electro-Mechanical System (MEMS) Accelerometer is being used as it is simple, reliable and highly cost effective. It consists of a cantilever beam along with a seismic mass which deflects due to an applied acceleration. This deflection is measured using analog or digital techniques and will be a measure of the acceleration applied.
![MEMS](https://www.fierceelectronics.com/s3/files/styles/breakpoint_xl_880px_w/s3/fierceelectronics/1562875377/capacitive_accelerometer.png/capacitive_accelerometer.png?itok=VTz2SByQ)

## Accelerometer Specifications
- Frequency Response – This parameter can be found out by analyzing the properties of the quartz crystal used and also the resonance frequency of the device.
- Accelerometer Grounding – Grounding can be in two modes. One is called the Case Grounded Accelerometer which has the low side of the signal connected to their core. This device is susceptible to ground noise. Ground Isolation Accelerometer refers to the electrical device kept away from the case. Such a device is prone to ground produced noise.
- Resonant Frequency – It should be noted that the resonant frequency should be always higher the the frequency response.
- Temperature of Operation – An accelerometer has a temperature range between -50 degree Celsius to 120 degree Celsius. This range can be obtained only by accurate installment of the device.
- Sensitivity – The device must be designed in such a way that it has higher sensitivity. That is, even for a small accelerative force, the electrical output signal should be very high. Thus a high signal can be measured easily and is sure to be accurate.
- Axis – Most of the industrial applications requires only a 2-axis accelerometer. But if you want to go for 3D positioning, a 3-axis accelerometer will be needed.
- Analog/Digital Output – You must take special care in choosing the type of output for the device. Analog output will be in the form of small changing voltages and digital output will be in PWM mode.

## Device Selection
Selection of the device depends on the following factors.

1. Selection depends on the range of frequency you need.
2. Depends on the size and shape of the object whose acceleration is to be measured.
3. Whether the measurement environment is dirty or clean.
4. Depends on the range of vibration that is to be measured.
5. Depends on the range of temperature in which the device will have to work.
6. Depends on whether the device is to be case grounded or grounded isolated.

## Applications
1. Machine monitoring.
2. Used to measure earthquake activity and aftershocks.
2. Used in measuring the depth of CPR chest compression.
4. Used in Internal Navigation System (INS). That is, measuring the position, orientation, and velocity of an object in motion without the use of any external reference.
5. Used in airbag shooting in cars and vehicle stability control.
6. Used in video games like PlayStation 3, so as to make the steering more controlled, natural and real.
7. Used in camcorder to make images stable.

# Types of Accelerometers in Market

![Types](https://www.analog.com/-/media/images/analog-dialogue/en/volume-51/number-4/articles/how-to-choose-the-most-suitable-accelerometer-for-your-application-part-1/139841_fig_01.png?la=en)

- LOW-COST ICP®
- PRECISION ICP®
- MULTI-AXIS ICP®
- DIGITAL OUTPUT
- HIGH TEMPERATURE
- HAZARDOUS AREA APPROVED
- CRYOGENIC ICP®
- RADIATION HARDENED
- LOW POWER ICP®
- EMBEDDABLE
- HANDHELD VIBRATION METER
etc.


## HANDHELD VIBRATION METER

- CONVENIENTLY MEASURES VIBRATION LEVELS OF INDUSTRIAL MACHINERY FOR PREDICTIVE MAINTENANCE REQUIREMENTS
- Puts predictive maintenance into the hands of machinery operators. Simple enough to use with minimal staff training, it conveniently measures the vibration levels of bearings, gears, and spindles for predictive maintenance requirements. 
- The kit is supplied with an industrial accelerometer, a cable assembly, a high-strength mounting magnet, and headphones for audible monitoring.

<br> **Handheld vibration meter kit includes**: 687A02 meter, 603C01 sensor, 050BQ006AC cable, 070A47 headphones, 080A131 magnet

**MODEL: 687A01 HANDHELD VIBRATION METER Specifications**


- Acceleration Range: 0.01 - 19.99 g rms (0.01 - 19.99 g rms)
- Velocity Range: 0.001 - 1.999 in/sec rms (NA)
- Frequency Response: (±3dB) 3000 to 3000000 cpm (3000 to 3000000 cpm)


**MODEL: 687A02 HANDHELD VIBRATION METER Specifications**


- Complies with ISO 2954 & ISO 10816
- Handheld vibration meter with BNC jack input and 1/8" stereo jack headphone connection. Available with 10 hour alkaline battery or 3 hour NiCd rechargable option.
- Acceleration Range: 0.01-199.99 g rms (0.01-19.99 g rms)
- Velocity Range: 0.001-1.999 in/sec rms (NA)
- Frequency Response: (+/-3dB) 3000 to 3000000 cpm (99 to 3000000 cpm)


## HIGH TEMPERATURE ACCELEROMETERS
- Often used in demanding industrial environments, these accelerometers and pressure sensors provide critical data that prevents failures and reduces downtime. 
- The high temperature ICP® accelerometers are capable of withstanding continuous temperatures of 325 ºF (162 ºC). 
- For applications that exceed those temperatures, IMI Sensors has a variety of charge mode accelerometers with integral ICP® amplifier that can operate at 900 ºF (482 ºC) and charge mode accelerometers without integral ICP® amplifier that can operate at 1200 ºF (649 ºC).


**MODEL: HT602D61 HIGH TEMPERATURE ACCELEROMETERS Specifications**

- High temperature, low profile, industrial, ceramic shear ICP® accel., 100 mV/g, 0.8 to 8000 Hz, side exit, integral armored cable, single point ISO 17025 accredited calibration.
- Capable of withstanding continuous temperatures of 325 °F (162 °C) without decay in performance, higher than any other IEPE accelerometer in the industry.
- Sensitivity: (±10%) 100 mV/g (10.2 mV/(m/s²))
- Frequency Range: (±3dB) 48 to 480000 cpm (0.8 to 8000 Hz)
- Sensing Element: Ceramic
- Measurement Range: ±50 g (±490 m/s²)
- Weight: 5.4 oz (153 gm)


